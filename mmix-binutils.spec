%global run_testsuite 0

Name:           mmix-binutils
Version:        2.25.1
Release:        1%{?dist}
Summary:        Cross-compiled version of binutils for MMIX machine code

License:        GPLv2+ and LGPLv2+ and GPLv3+ and LGPLv3+
Group:          Development/Libraries
URL:            http://www.gnu.org/software/binutils/
Source0:        http://ftpmirror.gnu.org/binutils/binutils-%{version}.tar.bz2
Patch0:         binutils-textdomain.patch


BuildRequires:  gettext-devel
BuildRequires:  flex
BuildRequires:  bison
BuildRequires:  texinfo
BuildRequires:  zlib-devel
%if %{run_testsuite}
BuildRequires:  dejagnu
BuildRequires:  sharutils
%endif
Provides:       bundled(libiberty)

%description
Cross compiled binutils (utilities like 'strip', 'as', 'ld') which
understand MMIX executables and object code.


%prep
%setup -q -n binutils-%{version}
%patch0 -p1


%build
mkdir build
pushd build
CFLAGS="$RPM_OPT_FLAGS" \
../configure \
  --build=%_build --host=%_host \
  --target=mmix \
  --prefix=%{_prefix} \
  --bindir=%{_bindir} \
  --includedir=%{_includedir} \
  --libdir=%{_libdir} \
  --mandir=%{_mandir} \
  --infodir=%{_infodir} \
  --disable-gdb \
  --disable-libdecnumber \
  --disable-readline \
  --disable-sim

make all %{?_smp_mflags}
popd


%check
%if !%{run_testsuite}
echo ====================TESTSUITE DISABLED=========================
%else
pushd build
  make -k check < /dev/null || :
  echo ====================TESTING MMIX =========================
  cat {gas/testsuite/gas,ld/ld,binutils/binutils}.sum
  echo ====================TESTING MMIX END=====================
  for file in {gas/testsuite/gas,ld/ld,binutils/binutils}.{sum,log}
  do
    ln $file binutils-mmix-knuth-mmixware-$(basename $file) || :
  done
  tar cjf binutils-mmix-knuth-mmixware.tar.bz2 binutils-mmix-knuth-mmixware-*.{sum,log}
  uuencode binutils-mmix-knuth-mmixware.tar.bz2 binutils-mmix-knuth-mmixware.tar.bz2
  rm -f binutils-mmix-knuth-mmixware.tar.bz2 binutils-mmix-knuth-mmixware-*.{sum,log}
popd
%endif


%install
make -C build install DESTDIR=$RPM_BUILD_ROOT

# These files conflict with ordinary binutils.
rm -rf $RPM_BUILD_ROOT%{_infodir}

%find_lang mmix-binutils
%find_lang mmix-bfd
%find_lang mmix-gas
%find_lang mmix-gprof
%find_lang mmix-ld
%find_lang mmix-opcodes
cat mmix-bfd.lang >> mmix-binutils.lang
cat mmix-gas.lang >> mmix-binutils.lang
cat mmix-gprof.lang >> mmix-binutils.lang
cat mmix-ld.lang >> mmix-binutils.lang
cat mmix-opcodes.lang >> mmix-binutils.lang


%files -f mmix-binutils.lang
%doc COPYING
%{_bindir}/mmix-addr2line
%{_bindir}/mmix-ar
%{_bindir}/mmix-as
%{_bindir}/mmix-c++filt
%{_bindir}/mmix-elfedit
%{_bindir}/mmix-gprof
%{_bindir}/mmix-ld
%{_bindir}/mmix-ld.bfd
%{_bindir}/mmix-nm
%{_bindir}/mmix-objcopy
%{_bindir}/mmix-objdump
%{_bindir}/mmix-ranlib
%{_bindir}/mmix-readelf
%{_bindir}/mmix-size
%{_bindir}/mmix-strings
%{_bindir}/mmix-strip
%dir %{_prefix}/mmix
%dir %{_prefix}/mmix/bin
%dir %{_prefix}/mmix/lib
%{_prefix}/mmix/bin/ar
%{_prefix}/mmix/bin/as
%{_prefix}/mmix/bin/ld
%{_prefix}/mmix/bin/ld.bfd
%{_prefix}/mmix/bin/nm
%{_prefix}/mmix/bin/objcopy
%{_prefix}/mmix/bin/objdump
%{_prefix}/mmix/bin/ranlib
%{_prefix}/mmix/bin/strip
%{_prefix}/mmix/lib/ldscripts
%{_mandir}/man1/mmix-*


%changelog
* Wed Aug 19 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 2.25.1-1
- Initial RPM release
